## Questions 1 (10 points total)
* a) 3 points total
  * 1 point for each correct answer
* b) same as (a)
* c) 4 points total
  * 1 point for correctly finding each digit

## Question 2 (10 points total)
* a) 0-5 points at your discretion
* b) 0-5 points at your discretion

## Question 3 (10 points total)
* a)
  * 5 points if correct
  * 0 points otherwise
* b)
  same as (a)

## Question 4 (10 points total)
0-10 points at your discretion

## Question 5 (8 points total)
* a)
  * 2 points if correct
  * 1 point if there is a small mistake
  * 0 points otherwise
* b,c,d) same as (a)
