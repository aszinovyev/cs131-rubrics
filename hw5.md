## Questions 1,3,4,5 (12 points each)
* 2 points for proving the basis case is true
* 2 points for correctly stating what needs to be proven in the inductive step
If the student doesn't state this, but it's clear he tried to prove the right
thing, still give 2 points
* 0-8 points for the proof of the inductive step (at your discretion).

## Question 2 (12 points)
* a) 4 points total
  * 1 point for the correct answer
* b) 8 points total
  * 1 point if both the answer is correct and the justification is reasonable
  * 0.5 points if only the answer is correct
  * 0 points otherwise
