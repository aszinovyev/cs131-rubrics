## Questions 1 (12 points)
* 2 points each
* 2 points for solutions logically equivalent to our solution
* 1 point for minor mistakes
* 0 points otherwise

## Questions 2 (12 points)
* 3 points each
* -1 if minor mistake
  * Missing “freshman classes”
  * Includes a negation symbol before a quantifier in part (d)
* No credit if wrong
* We had a mistake in the problem statement for part (b).
Therefore, give full credit for "this doesn't make sense" and
"this is a tautology" answers. Otherwise, check with our solutions posted on
piazza.

## Questions 3 (16 points)
* 2 points for each of the 8 subproblems
* 2 points if correct and justified
* 1 point if correct
* 0 points if incorrect

## Questions 4 (12 points)
3 points each.

Consider $`E(x,s)`$ and $`F(x,y)`$ to be undefined if the parameters are not
the right "type".
I.e., they can take any value (True or False) depending on the parameters.
For example, if $`s`$ is a course, then $`F(s,x)`$ can be True and $`F(s,y)`$
can be False.

If the student's answer is provably equivalent to our solution, give full
credit.
If the student only misses necessary conditions ($`C(s)`$, $`S(x)`$) or if their
solution fails only for weird cases (e.g. there exist no students or there exist
no courses), give 2 points out of 3.

Otherwise, give 1 or 0 points.
Use your judgement.
