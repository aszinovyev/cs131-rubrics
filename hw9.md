## Questions 1 (12 points total)
* a)
  * 6 points if the answer is correct
  * 0 points otherwise
* b)
  * 6 points if correct and justified
  * 4 points if justified but there is a small mistake
  * 0 points otherwise

## Question 2 (12 points total)
The student is supposed to show an example of a graph on 5 vertices that doesn't
contain a clique of order 3 and that doesn't contain an independent set of
vertices of size 3.
Give 12 points if the example is correct, 0 otherwise.

## Question 3 (12 points total)
* a) 4 points
  * 3 points for constructing a bijection
  * 1 point for the correct final answer
* b) 4 points
  * 4 points if correct and justified
  * 0 points otherwise
* c) 4 points
  * 4 points if the answer $`3^n`$ was found and justified
  * 3 points if the steps are justified but there is a small mistake
  * 2 points if partially correct; for example the expression
  $`\sum_{k=0}^{n}2^k \binom{n}{k}`$ was found.
  * 0 points otherwise

## Question 4 (12 points total)
* a,b,c,d)
  * 3 points for correctly defining a bijection and calculating the size of
  the set
  * 2 points for if a correct bijection is found but the answer is wrong
  * 1 point if the bijection is incorrect but the student went in the right
  direction
  * 0 points otherwise

## Question 5 (12 points total)
* a,b,c)
  * 4 points if the answer is correct and justified
  * 2 points if there is a small mistake
  * 0 points otherwise
